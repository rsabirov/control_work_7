import React, {Component} from 'react';
import './order-builder.css';
import OrderDetails from "./order-details/OrderDetails";
import Products from "./add-items/Products";

class OrderBuilder extends Component {

  state = {
    products: [
        {name: 'Hamburger', price: 80, id: 'i1'},
        {name: 'Cheeseburger', price: 90, id: 'i2'},
        {name: 'Fries', price: 45, id: 'i3'},
        {name: 'Coffee', price: 70, id: 'i4'},
        {name: 'Tea', price: 50, id: 'i5'},
        {name: 'Cola', price: 40, id: 'i6'},
      ],
    orderedProducts: []
  };

  addOrderItem = (product) => {
    let orderedProducts;
    const index = this.state.orderedProducts.findIndex(item => item.id === product.id);

    if (index === -1){
      orderedProducts = [...this.state.orderedProducts, {...product, amount: 1}];
    } else {
      orderedProducts = this.state.orderedProducts.map(item => {
        if (item.id === product.id) {
          return {...item, amount: item.amount + 1}
        }
        return item;
      });
    }

    this.setState({orderedProducts});
  };

  removeOrderItem = (id) => {
    this.setState({
      orderedProducts: this.state.orderedProducts.filter(product => {
        return product.id !== id;
      })
    });
  };


  render() {
    return (
      <div className="container">
        <OrderDetails orderedProducts={this.state.orderedProducts} remove={this.removeOrderItem} />
        <Products products={this.state.products} add={this.addOrderItem}/>
      </div>
    )
  }
}

export default OrderBuilder;