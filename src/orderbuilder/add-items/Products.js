import React from 'react';
import './add-items.css';
import Product from "./item/Product";

const Products = (props) => {
  return (
    <div className="add-items">
      <h2 className="add-items__title">Add items:</h2>
      <div className="add-items__wrapper">
        {props.products.map((product) =>
          <Product product={product} click={props.add} key={product.id} />
        )}
      </div>
    </div>
  )
};

export default Products;