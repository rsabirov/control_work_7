import React from 'react';

const Product = (props) => {
  const product = props.product;
  return (
    <div className="add-items__item" onClick={() => props.click(product)}>
      <h3 className="add-items__item-title">
        {product.name}
      </h3>
      <div className="add-items__description">
        <p className="add-items__label">Price:</p>
        <span className="add-items__price">{product.price} KGS</span>
      </div>
    </div>
  );
};

export default Product;