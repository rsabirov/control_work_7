import React from 'react';
import './order-details.css';
import DetailItem from "./detail-item/DetailItem";

const OrderDetails = (props) => {
  const total = props.orderedProducts.reduce((total, product) => {
    return total + product.price * product.amount;
  }, 0);

  return (
    <div className="details">
      <h2 className="details__title">Order details:</h2>
      {props.orderedProducts.map(product =>
        <DetailItem key={product.id} product={product} click={props.remove} />
      )}
      <div className="total">
        <div className="total__label">
          Total price:
        </div>
        <div className="total__amount">
          {total} KGS
        </div>
      </div>
    </div>
  )
};

export default OrderDetails;