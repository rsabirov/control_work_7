import React from 'react';

const DetailItem = (props) => {
  const product = props.product;
  return (
    <div className="details__item">
      <span className="details__label">{product.name}</span>
      <p className="details__amount">x {product.amount}</p>
      <span className="details__price">{product.price * product.amount} KGS</span>
      <button className="details__remove" onClick={() => props.click(product.id)}>Delete</button>
    </div>
  )
};

export default DetailItem;